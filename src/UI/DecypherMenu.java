package UI;

import java.util.Scanner;

import enigma.Enigma;

/**
 * Sub class of Menu. Mandatory methods are overwritten.
 * @author Siim Haas
 *
 */
public class DecypherMenu extends Menu{

	public DecypherMenu(Enigma _enigma) {
		super(_enigma);
	}

	@Override
	protected void printMenu() {
		System.out.print("Enter message to decypher: ");
		getUserChoice();
	}

	@Override
	protected void getUserChoice() {
		Scanner sc = new Scanner(System.in);
		String msgFromUser = sc.nextLine();
		// check input
		while (true) {
			if (isValid(msgFromUser)) {
				performActions(msgFromUser);
				break;
			} else {
				System.out.println("Invalid input! Valid characters are (uppercase): ABCDEFGHIJKLMNOPQRSTUVWXYZ");
				System.out.print("Enter message to decypher: ");
				msgFromUser = sc.nextLine();
			}
		}
		enigma.enterEngimaMenu();
	}

	/**
	 * Method prints out ciphered text and plain text
	 * @param msg - ciphered text
	 */
	protected void performActions(String msg) {
		System.out.println(msg + " => " + enigma.returnCipheredText(msg));
	}
	
	/**
	 * Validates user input
	 * @param incomingMsg
	 * @return boolean whether input is valid or not
	 */
	private boolean isValid(String incomingMsg) {
		
		boolean result = false;
		
		for (char inMsg : incomingMsg.toCharArray()) {
			for(int i = 65; i < 91; i++){
				if((char)i == inMsg){
					result = true;
					break;
				}
			}
			if(result == false){
				return result;
			}
		}
		return result;
	}

	@Override
	protected void performActions() {
	
	}
}
