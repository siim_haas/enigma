package UI;

import java.util.Scanner;

import enigma.Enigma;

/**
 * Main menu designed for Enigma. Main menu has sub menus. 
 * 
 * @author Siim Haas
 *
 */
public class MainMenu extends Menu{
	
	Menu cipherMenu;
	Menu decipherMenu;
	Menu setRotorsPositions;
	Menu getRotorsPositions;
	
	public MainMenu(Enigma _enigma){
		super(_enigma);
		cipherMenu = new CipherMenu(_enigma);
		decipherMenu = new DecypherMenu(_enigma);
		setRotorsPositions = new SetRotorsPositionsMenu(_enigma);
		getRotorsPositions = new GetRotorsPositionsMenu(_enigma);
	}
	
	protected void printMenu() {
		System.out.println("Welcome to engima simulator!");
		System.out.println("Your choices are:");
		System.out.println("(1) Cipher message");
		System.out.println("(2) Decipher message");
		System.out.println("(3) Set rotors' positions");
		System.out.println("(4) Get rotors' positions");
		System.out.println("(5) Quit");
		System.out.println("What do you want to do?");
		getUserChoice();
	}

	@Override
	protected void getUserChoice() {
		Scanner sc = new Scanner(System.in);
		choice = sc.nextInt();
		while(true){								
			if(choice >=1 && choice <= 5){
				break;
			}else{
				System.out.println("Invalid input! [1 ... 5]");
				System.out.println("What do you want to do?");
				choice = sc.nextInt();				
			}
		}		
		performActions();
	}

	@Override
	protected void performActions() {		
		switch (choice) {
		case 1:
			cipherMenu.enterMenu();
			break;
		case 2:
			decipherMenu.enterMenu();
			break;
		case 3:
			setRotorsPositions.enterMenu();
			break;
		case 4:
			getRotorsPositions.enterMenu();
			break;
		case 5:
			System.out.println("Quitting program ...");
			break;
		default:
			break;
		}
	}
}