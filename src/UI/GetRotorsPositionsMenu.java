package UI;

import enigma.Enigma;

/**
 * Menu to get rotors' positions
 * @author Siim Haas
 *
 */
public class GetRotorsPositionsMenu extends Menu{

	public GetRotorsPositionsMenu(Enigma _enigma) {
		super(_enigma);
	}

	@Override
	protected void printMenu() {
		enigma.getRotorPosition();
		enigma.enterEngimaMenu();
	}

	@Override
	protected void getUserChoice() {
		
	}

	@Override
	protected void performActions() {
		
	}

}