package UI;

import enigma.Enigma;

/**
 * Abstract class for Menus
 * Each menu has to have following methods:
 * printMenu(), getUserChoice(), performActions()
 * 
 * public access is given to enterMenu()
 * @author Siim Haas
 *
 */
public abstract class Menu {
	
	/** Field to remember general user input */
	protected int choice;
	/** Each menu is connected to the same Enigma machine */
	final protected Enigma enigma;
	
	/**
	 * Constructor where menu is connected to Enigma object.
	 * Choice is initialized.
	 * @param _enigma - constructor requires Enigma object to be connected to
	 */
	public Menu(Enigma _enigma){
		choice = 0;
		enigma = _enigma;
	}
	
	/**
	 * Each menu must have method to print itself.
	 */
	abstract protected void printMenu();
	
	/**
	 * Each menu must have method to get user input. Validation is also done here.
	 */
	abstract protected void getUserChoice();
	
	/**
	 * Each menu must have method to perform action based on user input.
	 */
	abstract protected void performActions();
	
	/**
	 * Each menu must be accessed outside of class.
	 */
	public void enterMenu(){
		printMenu();
	}
}
