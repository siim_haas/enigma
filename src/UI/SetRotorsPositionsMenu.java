package UI;

import java.util.Scanner;

import enigma.Enigma;

/**
 * Menu to set rotors' positions.
 * @author Siim Haas
 *
 */
public class SetRotorsPositionsMenu extends Menu {

	public SetRotorsPositionsMenu(Enigma _enigma) {
		super(_enigma);
				
	}

	@Override
	protected void printMenu() {
		for(int i = 0; i < 3; i++){
			System.out.println("Set position of rotor " + (i+1)+": ");
			getUserChoice(i);
		}
		enigma.enterEngimaMenu();
	}

	
	protected void getUserChoice(int i) {		
		Scanner sc = new Scanner(System.in);
		choice = sc.nextInt();
		while(true){
			if(choice >= 0 && choice <=25)
				break;
			else{
				System.out.println("Invalid input! Position = [0 ... 25]");
				System.out.println("Set position of rotor " + (i+1)+": ");
				choice = sc.nextInt();
			}
		}
		performActions(i);
	}

	protected void performActions(int i) {
		enigma.setRotorPosition(i, choice);
	}

	@Override
	protected void getUserChoice() {
		
	}

	@Override
	protected void performActions() {

	}

}