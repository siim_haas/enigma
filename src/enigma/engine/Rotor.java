package enigma.engine;

import java.util.LinkedList;

/**
 * Engine for Enigma. Includes cryptographic algorithm and other functionality.
 * @author Siim Haas
 * 
 */

public class Rotor {
	
	/** List where rotor connections are held as integers for simple handling */
	LinkedList<Integer> wiring = new LinkedList<Integer>();
	
	/** Each rotor can be connected to next rotor */
	private Rotor next;
	
	/** Each rotor can be connected to previous rotor */
	private Rotor previous;
	
	/** Each rotor can be turned and current position is held here */
	private int position;
	
	/** Each rotor must turn next rotor when turning point is passed*/
	private char turningPoint;
	
	/** Each rotor can be reflector*/
	private boolean reflector;
	
	/** Flag for double-step situation*/
	private static boolean doubleStep = false; // flag for double stepping situation
	
	/**
	 * Constructor for creating regular rotor. Fields are initialized.
	 * @param wiringString - list of connections. Must be validated before.
	 * @param t - turning point
	 */
	public Rotor(String wiringString, char t) {
		for (char c : wiringString.toCharArray()) {
			wiring.add((int) c - 65);
		}
		turningPoint = t;
		reflector = false;
		setPosition(0);
	}

	/**
	 * Constructor for creating reflector type rotor. Fields are initialized.
	 * @param wiringString - list of connections. Must be validated before.
	 */
	public Rotor(String wiringString) {
		for (char c : wiringString.toCharArray()) {
			wiring.add((int) c - 65);
		}
		reflector = true;
		setPosition(0);
	}
	
	/**
	 * Method to connect two rotors. Connection is two way.
	 * @param r - this rotor is connected to passed rotor r
	 */
	public void connectRotors(Rotor r) {
		this.next = r;
		r.previous = this;
	}
	
	/**
	 * Entrance to cryptographic algorithm. It calls out actual private algorithm. 
	 * @param i - Character that is casted as integer. Must be validated before
	 * @param comingFromRotor - Rotor where plain text is passed in
	 * @return - returns ciphered text as integer. Can be casted later as character.
	 */
	public int doEnigma(int i, Rotor comingFromRotor) {
		return cipher(i, comingFromRotor);
	}
	
	/**
	 * Actual cryptographic algorithm. Method passes plain text through this rotor and calls out same method for next rotor.
	 * Method is recursive.
	 * If comingFromRotor == null then it means that is this is the first rotor where ciphering begins.
	 * Therefore initial direction can be determined.
	 * Direction is essential to decide parameters to pass next(direction == forward) or previous(direction == backwards) rotor.
	 * If plain or cycled ciphered text is passed on then the following aspects must be accounted for:
	 * 1) If forward - get value at <index>, if backward - return index of <value>
	 * 2) Take positions into account. Position are related to each other. Subtract previous rotor position and add this position
	 * 3) relative position can be greater than 25 so mod 26 is required.
	 * @param i - Character that is casted as integer. Must be validated before
	 * @param comingFromRotor - Rotor where plain text is passed in
	 * @return - returns ciphered text as integer. Can be casted later as character.
	 */
	private int cipher(int i, Rotor comingFromRotor) {
		
		if (comingFromRotor != null) {
			
			//Condition determines forward direction (before reflector)
			if (this.next != null && comingFromRotor.next == this) {				
				return next.cipher(this.wiring.get((i - this.previous.getPosition() + this.getPosition() + 26) % 26), this);

			//Condition determines backward direction(after reflector) or when it has reached reflector
			} else if (this.previous != null) {

				if (this.reflector == true) {				
					return previous.cipher((this.wiring.indexOf((i - this.previous.getPosition() + this.getPosition() + 26) % 26)), this);
				} else {					
					return previous.cipher(this.wiring.indexOf((i - this.next.getPosition() + this.getPosition() + 26) % 26), this);
				}
			
			//Condition determines end of line. Returns final ciphered text.
			} else if (this.previous == null && comingFromRotor.previous == this) {				
				return (this.wiring.indexOf((i + this.getPosition() - this.next.getPosition() + 26) % 26) - this.getPosition() + 26) % 26;				
			} else {
				return this.wiring.get(i + this.getPosition());
			}
		
		// Plain text gets here, because doEngima(char c, comingFromRotor == null)
		} else { 		
			return next.cipher(this.wiring.get((i + this.getPosition() + 26) % 26), this);
		}
	}

	/**
	 * Method to turn each rotor as algorithm requires
	 * Next rotor is checked first and turned before this rotor
	 * Reflector is never turned
	 */
	public void turnRotor() {

		if (this.turningPoint == (char) (this.getPosition() + 65) && next != null || doubleStep == true) {
			doubleStep = false;
			next.turnRotor();
		}

		if (this.previous != null && this.next != null && this.next.reflector != true && this.turningPoint == (char) (this.getPosition() + 1 + 65)) {
			doubleStep = true;
		}
		if (this.reflector != true) {
			if (this.getPosition() == 25)
				this.setPosition(0);
			else
				this.setPosition(this.getPosition() + 1);
		}
	}
	
	/**
	 * Encapsulating position field
	 * @return position
	 */
	public int getPosition() {
		return position;
	}

	/**
	 * Encapsulating position field
	 * @param position - value to be given to position 
	 */
	public void setPosition(int position) {
		this.position = position;
	}
}
