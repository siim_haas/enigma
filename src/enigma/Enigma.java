package enigma;

import UI.MainMenu;
import UI.Menu;
import enigma.engine.Rotor;

/**
 * Class for creating Enigmas. Each Enigma has three rotors (r1, r2, r3) and reflector
 * Connections are made from right to left: reflector<-r1<-r2<-r3
 * Plain text or ciphered text is push in r3 and from there it is processed in cryptographic algorithm   
 * 
 * @author Siim Haas
 * 
 */
public class Enigma {
	/** Three rotors and a reflector for each Engima. Reflector is rotor object, but with different constructor */
	private Rotor r1, r2, r3, reflector;
	/** Each Enigma has menu for user interface */
	private Menu menuForEnigma;
	
	/**
	 * Constructor where rotors and reflector are created and connections made.
	 * Also menu(MainMenu) for Enigma is created. 
	 */
	public Enigma() {	
		
		r1 = new Rotor("EKMFLGDQVZNTOWYHXUSPAIBRCJ", 'Q');
		r2 = new Rotor("AJDKSIRUXBLHWTMCQGZNPYFVOE", 'E');
		r3 = new Rotor("BDFHJLCPRTXVZNYEIWGAKMUSQO", 'V');
		reflector = new Rotor("YRUHQSLDPXNGOKMIEBFZCWVJAT");

		r3.connectRotors(r2);
		r2.connectRotors(r1);
		r1.connectRotors(reflector);
		
		menuForEnigma = new MainMenu(this);
	}

	/**
	 * Any text that is entered in enigma comes here first.
	 * String is turned into toCharArray() and each character is passed individually through Enigma.  
	 * @param messageToCipher - Plain text as string.
	 * @return Ciphered/Deciphered message as string. 
	 */
	public String returnCipheredText(String messageToCipher) {

		String cipheredMessage = "";
		for (char c : messageToCipher.toCharArray()) {
			r3.turnRotor();
			cipheredMessage = cipheredMessage
					+ (char) (r3.doEnigma((int) (c - 65), null) + 65);
		}
		return cipheredMessage;
	}
	
	/**
	 * Sets rotor positions. Default settings are 0, 0, 0.
	 * @param i - determines rotor to set. 0 - r1, 1 - r2, 2 - r3 
	 * @param position - desired rotor setting. Expects that passed setting is already validated
	 */
	public void setRotorPosition(int i, int position){
		switch (i+1){
			case 1:
				r1.setPosition(position);
				break;
			case 2:
				r2.setPosition(position);
				break;
			case 3:
				r3.setPosition(position);
				break;
			default:
				r1.setPosition(0);
				r2.setPosition(0);
				r3.setPosition(0);
				break;
		}				
	}

	/**
	 * Prints out each rotor position.
	 */
	public void getRotorPosition(){
		System.out.println("Rotor 1: " + r1.getPosition());
		System.out.println("Rotor 2: " + r2.getPosition());
		System.out.println("Rotor 3: " + r3.getPosition());
	}
	
	/**
	 * Public access to Enigma menu.
	 */
	public void enterEngimaMenu(){
		menuForEnigma.enterMenu();
	}
}